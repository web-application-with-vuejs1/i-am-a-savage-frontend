/* eslint-disable eol-last */
/* eslint-disable padded-blocks */
/* eslint-disable space-before-function-paren */
/* eslint-disable indent */
import api from './api'

export function login(email, password) {
    return api.post('auth/login', { username: email, password: password })
}